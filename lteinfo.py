#!/usr/bin/env python
# coding: utf-8
import serial

LTE_CONSOLE_PATH = "/dev/ttyUSB0"
def measure_lte():
    try:
        with serial.Serial(
            LTE_CONSOLE_PATH,
            baudrate=115200,
            timeout=5
        ) as ser:
            parse_lte_console(ser)
    except Exception as detail:
        print(",,,,,,,,,,,")
        lte_console_path = None

def parse_lte_console(console):
    global lte
    console.write("AT!GSTATUS?\r")
    lte = dict()
    value_map = {
        "LTE bw": "Bandwidth",
        "PCC RxM RSSI": "RSSI",
        "PCC RxM RSRP": "RSRP",
        "RSRQ (dB)": "RSRQ",
        "SINR (dB)": "SINR Rx[0]",
    }
    while True:
        line = console.readline()
        if len(line) == 4:  # this is "OK\r\n"
            break
        for value_pair in line.split("\t"):
            value_pair = value_pair.split(":")
            value_name = value_map.get(value_pair[0], None)
            if value_name:
                lte[value_name] = value_pair[1].split()[0]
    console.write("AT!LTEINFO?\r")
    while True:
        line = console.readline()
        if len(line) < 3:
            continue
        split_line = line.split()
        if len(line) == 4:  # this is "OK\r\n"
            break
        if split_line[0] == "Serving:":
            line = console.readline()
            split_line = line.split()
            lte["MCC"] = split_line[1]
            lte["MNC"] = split_line[2]
            lte["LAC"] = split_line[3]
            lte["Cell Id"] = int(split_line[4], 16)
            lte["PCI"] = split_line[9]
        if split_line[0] == "InterFreq:" and split_line[1] == "EARFCN":
            line = console.readline()
	    try:
                lte["DL EARFCN"] = line.split()[0]
	    except:
	        lte["DL EARFCN"] = "0"

    print(lte["RSRP"]+","+lte["SINR Rx[0]"]+","+lte["RSRQ"]+","+lte["RSSI"]+",$KBPS,"+lte["MCC"]+","+lte["MNC"]+","+lte["LAC"]+","+str(lte["Cell Id"])+","+lte["PCI"]+","+str(lte["DL EARFCN"])+","+lte["Bandwidth"])
def main():
    measure_lte()

if __name__ == "__main__":
    main()

