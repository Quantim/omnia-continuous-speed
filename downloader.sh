#!/bin/bash

DOWNLOAD_SOURCE=http://cdimage.ubuntu.com/releases/18.04/release/ubuntu-18.04.1-server-arm64.template
IFACE=wwan0

wget -O /dev/null -o /dev/null $DOWNLOAD_SOURCE &
PID=$!

trap "kill $PID 2> /dev/null" EXIT
echo "Time,Lat,Long,RSRP,SINR Rx[0],RSRQ,RSSI,IP Thrpt DL,MCC,MNC,LAC,Cell Id,PCI,DL EARFCN,Bandwidth,QPSK Rate,16-QAM Rate,64-QAM Rate,256-QAM Rate,Carrier Aggregation DL"
OLD_COUNT=$(cat /sys/class/net/$IFACE/statistics/rx_bytes)
#require codeutils-date package
sleep 0.$(printf '%04d' $((10000 - 10#$(/usr/bin/date +%4N))))
while kill -0 $PID 2> /dev/null; do
    sleep 0.$(printf '%04d' $((10000 - 10#$(/usr/bin/date +%4N))))
    COUNT=$(cat /sys/class/net/$IFACE/statistics/rx_bytes)
    let "KBPS=($COUNT-$OLD_COUNT)*8/1000"
    RESLINE=$(date +"%d.%m.%Y %H:%M:%S")",,,"$(/root/lteinfo.py)",,,,"
    eval echo "$RESLINE"
    OLD_COUNT=$COUNT
done

# Disable the trap on a normal exit.
trap - EXIT
